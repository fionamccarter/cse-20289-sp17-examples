#!/usr/bin/env python2.7

# ZIPCODE.PY ###################################################################

import os
import re
import sys

import requests

# Global variables

STATE  = 'Indiana'
CITY   = None
FORMAT = 'text'

# Usage function

def usage(status=0):
    print '''Usage: {}
    -c      CITY    Which city to search
    -f      FORMAT  Which format (text, csv)
    -s      STATE   Which state to search (Indiana)'''.format(
        os.path.basename(sys.argv[0])
    )
    sys.exit(status)

# Parse command line options

args = sys.argv[1:]
while len(args) and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-c':
        CITY = args.pop(0)
    elif arg == '-f':
        FORMAT = args.pop(0)
    elif arg == '-s':
        STATE = args.pop(0)
    elif arg == '-h':
        usage(0)
    else:
        usage(1)

# Main execution

url      = 'http://www.zipcodestogo.com/{}/'.format(STATE)
response = requests.get(url)
regex    = '<a href="http://www.zipcodestogo.com/([^/]+)/[A-Z]{2}/([0-9]{5})/">'
results  = []

for city, zipcode in re.findall(regex, response.text):
    if CITY is None or city == CITY:
        results.append(zipcode)

if FORMAT == 'text':
    print '\n'.join(results)
else:
    print ','.join(results)
