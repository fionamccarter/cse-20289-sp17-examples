#!/usr/bin/env python2.7

import ctypes
import sys

hello = ctypes.CDLL('./libhello.so')

for arg in sys.argv[1:]:
    hello.hello(arg)
