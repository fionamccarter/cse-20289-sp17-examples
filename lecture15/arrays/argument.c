/* argument.c */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool contains(int *a, int n, int k) {
    for (size_t i = 0; i < n; i++)
    	if (a[i] == k)
    	    return true;
    return false;
}

int main(int argc, char *argv[]) {
    int a[]  = {5, 4, 7, 0, 1, 3, 13, 3, 15, 12, 24};
    size_t n = sizeof(a) / sizeof(int);

    for (size_t i = 0; i < 15; i++) {
    	if (contains(a, n, i)) {
    	    printf("Array contains %lu\n", i);
	} else {
    	    printf("Array does not contain %lu\n", i);
	}
    }

    return EXIT_SUCCESS;
}
