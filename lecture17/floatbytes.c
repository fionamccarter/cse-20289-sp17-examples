/* floatbytes.c */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

union Data {
    float   number;
    uint8_t bytes[sizeof(float)];
};

int main(int argc, char *argv[]) {
    union Data data;	// We can shorten this w/ type definition

    printf("sizeof(data) = %ld bytes\n", sizeof(data));

    for (int i = 1; i < argc; i++) {
	data.number = strtof(argv[i], NULL);

	for (int c = 0; c < sizeof(float); c++) {
	    printf("%d: %02x\n", c, data.bytes[c]);
	}
	putchar('\n');
    }

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
