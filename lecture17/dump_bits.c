/* dump_bits.c */

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#define DUMP_BITS(_v) \
    for (int i = sizeof(_v)*8 - 1; i >= 0; i--) {\
        if ((i + 1) % 8 == 0) putc(' ', stdout); \
        putc(((_v) & 1<<i) ? '1' : '0', stdout); \
    } \
    putc('\n', stdout);

int main(int argc, char *argv[]) {
    /* Signed and Unsigned are the same underlying bits */
    int         i = 0xDEADBEEF;             // Need to fix macro with _i
    unsigned    u = 0xDEADBEEF;

    DUMP_BITS(i);
    DUMP_BITS(u);
    printf("i = %d, u = %u, i == u ? %d\n", i, u, i == u);

    /* Need to be careful with overflowing integer range */
    for (int8_t b = 0; b < (1<<8); b++) {   // Overflows into infinite loop
    	DUMP_BITS(b);
    }

    for (int c = 0; c < (1<<8); c++) {      // Fix by using larger int
        int8_t b = c;
    	DUMP_BITS(b);
    }

    /* Intel machines are little endian. */
    int8_t *a = (int8_t *)&u;
    for (int i = 0; i < sizeof(u); i++)
        printf(" %hhx", a[i]);
    putc('\n', stdout);

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
