#!/usr/bin/env python2.7

import os
import re
import requests

import multiprocessing

def wget(url):
    p = os.path.basename(url)

    print 'Downloading {} to {}'.format(url, p)
    r = requests.get(url)
    with open(p, 'wb') as f:
        f.write(r.content)

BASE = 'http://proudtobe.nd.edu'
URL  = BASE + '/wallpapers/'

response   = requests.get(URL)
wallpapers = [BASE + asset for asset in re.findall('/assets/[^"]+', response.text)]
pool       = multiprocessing.Pool(multiprocessing.cpu_count())

pool.map(wget, wallpapers)
