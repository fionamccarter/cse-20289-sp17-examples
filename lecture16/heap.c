/* heap.c */

#include <ctype.h>
#include <errno.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

char *title_case_00(const char *s) {
    char buffer[BUFSIZ];

    strncpy(buffer, s, BUFSIZ);
    buffer[0] = toupper(buffer[0]);

    return buffer;  // ERROR: returning stack allocated buffer
}

char *title_case_01(const char *s) {
    char *buffer = malloc(sizeof(s));	// ERROR: wrong size

    strncpy(buffer, s, sizeof(s));	// ERROR: wrong size
    buffer[0] = toupper(buffer[0]);

    return buffer;
}

char *title_case_02(const char *s) {
    char *buffer = malloc(strlen(s));	// ERROR: still wrong size

    strncpy(buffer, s, strlen(s));	// ERROR: still wrong size
    buffer[0] = toupper(buffer[0]);

    return buffer;
}

char *title_case_03(const char *s) {
    char *buffer = malloc(strlen(s) + 1);

    strncpy(buffer, s, strlen(s));	
    buffer[0] = toupper(buffer[0]);

    return buffer;
}

char *title_case_04(const char *s) {
    char *buffer = strdup(s);		// Allocate and copy string

    buffer[0] = toupper(buffer[0]);
    return buffer;
}

int main(int argc, char *argv[]) {
    int *h = malloc(INT_MAX);

    /* Doesn't fail on Linux */
    if (h == NULL) {
    	fprintf(stderr, "malloc failed: %s\n", strerror(errno));
    	return EXIT_FAILURE;
    }

    /* Show ps ux: virtual vs physical memory */
    sleep(10);

    for (int i = 1; i < argc; i++) {
	char *result = title_case_04(argv[i]);
	puts(result);
	//free(result);
    }

    return EXIT_SUCCESS;
}
