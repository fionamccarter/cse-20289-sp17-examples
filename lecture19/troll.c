/* troll.c: Using signals for fun and profit */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>

void message(char **messages, size_t n) {
    char command[BUFSIZ];

    sprintf(command, "cowsay \"%s\"", messages[rand() % n]);
    system(command);
}

void rude() {
    static char *MESSAGES[] = {
        "Uh... What? What do you want?",
        "Leave me alone!",
        "Ugh, this again?",
        "Really, you are spending your time on this?",
        "Ain't nobody got time for this",
    };

    message(MESSAGES, sizeof(MESSAGES)/sizeof(char *));
}

void taunt(int signum) {
    static char *MESSAGES[] = {
	"Nice try noob.",
	"Lol, L2Unix!",
	"Oh, I am so scared.",
	"404",
	"Illegal Operation",
    };
    
    message(MESSAGES, sizeof(MESSAGES)/sizeof(char *));
}

void wake(int signum) {
    static char *MESSAGES[] = {
        "Wake me up, wake me up inside",
        "Tell me I'm not dreaming but are we out of time?",
        "If you fall I will catch you, I will be waiting, time after time",
    };
    
    message(MESSAGES, sizeof(MESSAGES)/sizeof(char *));
    exit(EXIT_SUCCESS);
}

int main(int argc, char *argv[]) {
    signal(SIGINT , taunt);     // Register handlers
    signal(SIGTERM, taunt);
    signal(SIGALRM, wake);

    srand(time(NULL));          // Seed random number generator

    rude();                     // Display rude message
    alarm(10);                  // Set alarm

    while (1) {
    	sleep(1);
    }

    return EXIT_SUCCESS;
}

/* vim: set sts=4 sw=4 ts=8 expandtab ft=c: */
